ifndef GITLAB_CI
# Running locally
PROJECT_BASE_PATH	:= $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
PROJECT_BASE_DIR	:= $(notdir $(PROJECT_BASE_PATH))
PROJECT_BUILD_PATH	:= $(PROJECT_BASE_PATH)/build
PROJECT_NAMESPACE	:= $(notdir $(shell vvv='$(PROJECT_BASE_PATH)'; echo "$${vvv%/*}"))
PROJECT_NAME		:= $(PROJECT_BASE_DIR)
DOCKER_TAG_LOCAL	:= $(PROJECT_NAMESPACE)/$(PROJECT_NAME):local
DOCKER_COMPOSE_ENV	:= PROJECT_NAMESPACE=$(PROJECT_NAMESPACE) PROJECT_NAME=$(PROJECT_NAME)
else
# Running on GitLab CI environment
PROJECT_NAMESPACE	:= $(CI_PROJECT_NAMESPACE)
PROJECT_NAME		:= $(CI_PROJECT_NAME)
PROJECT_RELEASE		:= $(shell echo "$(CI_COMMIT_TAG)" | egrep -q "^v[0-9]+\.[0-9]+\.[0-9]+$$" && echo $(CI_COMMIT_TAG))
endif

ifdef CI_COMMIT_TAG
ifdef PROJECT_RELEASE
# GitLab CI tagged commit with release version number
PATCH_VERSION		:= $(shell vvv='$(PROJECT_RELEASE)'; echo "$${vvv#?}")
MINOR_VERSION		:= $(shell vvv='$(PATCH_VERSION)'; echo "$${vvv%.*}")
MAJOR_VERSION		:= $(shell vvv='$(MINOR_VERSION)'; echo "$${vvv%.*}")
DOCKER_TAG_MAJOR	:= $(CI_REGISTRY_IMAGE):$(MAJOR_VERSION)
DOCKER_TAG_MINOR	:= $(CI_REGISTRY_IMAGE):$(MINOR_VERSION)
DOCKER_TAG_PATCH	:= $(CI_REGISTRY_IMAGE):$(PATCH_VERSION)
DOCKER_TAG_LATEST	:= $(CI_REGISTRY_IMAGE):latest
DOCKER_TAGS			:= --tag $(DOCKER_TAG_PATCH) --tag $(DOCKER_TAG_MINOR) --tag $(DOCKER_TAG_MAJOR) --tag $(DOCKER_TAG_LATEST)
DOCKER_TAGS_CSV		:= $(DOCKER_TAG_PATCH), $(DOCKER_TAG_MINOR), $(DOCKER_TAG_MAJOR), $(DOCKER_TAG_LATEST)
DOCKER_IMAGE		:= $(CI_REGISTRY_IMAGE)
else
# GitLab CI tagged commit with something else
DOCKER_TAG_TEST		:= $(CI_REGISTRY_IMAGE)/test:$(CI_COMMIT_TAG)
DOCKER_TAGS			:= --tag $(DOCKER_TAG_TEST)
DOCKER_TAGS_CSV		:= $(DOCKER_TAG_TEST)
DOCKER_IMAGE		:= $(DOCKER_TAG_TEST)
endif
else
ifndef DOCKER_TAG_LOCAL
# GitLab CI non-tagged commit
DOCKER_TAG_BRANCH	:= $(CI_REGISTRY_IMAGE)/$(CI_COMMIT_REF_NAME)
DOCKER_TAGS			:= --tag $(DOCKER_TAG_BRANCH)
DOCKER_TAGS_CSV		:= $(DOCKER_TAG_BRANCH)
DOCKER_IMAGE		:= $(DOCKER_TAG_BRANCH)
endif
endif

# Local commands

.PHONY: build run

build: docker

run: up

# Local docker commands

.PHONY: docker up up-build down

docker:
	@echo [docker-build] building image $(DOCKER_TAG_LOCAL)
	@docker build . \
		--tag $(DOCKER_TAG_LOCAL)

up:
	@$(DOCKER_COMPOSE_ENV) docker-compose up

up-build:
	@$(DOCKER_COMPOSE_ENV) docker-compose up --build

down:
	@docker-compose down

# GitLab CI commands

.PHONY: gitlab-ci

gitlab-ci:
	@if [ "X$(GITLAB_CI)" != "Xtrue" ]; then echo [gitlab-ci] only available on GitLab CI environment; exit 1; fi
	@echo "[gitlab-ci] logging in to registry $(CI_REGISTRY_IMAGE)"
	@echo $(CI_REGISTRY_PASSWORD) | docker login -u $(CI_REGISTRY_USER) --password-stdin $(CI_REGISTRY)
	@echo "[gitlab-ci] building image $(DOCKER_TAGS_CSV)"
	@docker build . \
		--tag $(DOCKER_IMAGE) \
		$(DOCKER_TAGS)
	@echo "[gitlab-ci] pushing image to registry"
	@docker push $(DOCKER_IMAGE)

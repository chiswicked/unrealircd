FROM debian:stable-slim
ARG UNREALIRCD_VERSION=4.2.3

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y \
        build-essential \
        openssl \
        libssl-dev \
        zlib1g \
        zlib1g-dev \
        zlibc \
        sudo \
        curl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /data \
    && useradd -r -d /data unrealircd \
    && chown unrealircd:unrealircd /data

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN cd /data \
    && sudo -u unrealircd curl -s --location https://www.unrealircd.org/unrealircd4/unrealircd-$UNREALIRCD_VERSION.tar.gz | sudo -u unrealircd tar xz \
    && cd unrealircd-$UNREALIRCD_VERSION \
    && sudo -u unrealircd sed -i "s|config src/ssl.cnf|subj '/CN=localhost'|g" Makefile.in \
    && sudo -u unrealircd ./Config \
        --with-showlistmodes \
        --with-listen=5 \
        --with-nick-history=2000 \
        --with-sendq=3000000 \
        --with-bufferpool=18 \
        --with-permissions=0600 \
        --with-fd-setsize=1024 \
        --enable-dynamic-linking \
    && sudo -u unrealircd make \
    && sudo -u unrealircd make install \
    && cd /data \
    && rm -rf unrealircd-$UNREALIRCD_VERSION \
    && chmod +x /data/unrealircd/unrealircd

COPY unrealircd.conf /data/unrealircd/conf/unrealircd.conf

# Standard IRC port 6667
EXPOSE 6667
# Standard IRC SSL/TLS port 6697
EXPOSE 6697
# Special SSL/TLS servers-only port for linking
EXPOSE 6900

USER unrealircd
ENTRYPOINT ["/data/unrealircd/bin/unrealircd", "-F"]
